package com.joyjet.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.joyjet.utils.extenstions.threadingSubscribe
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseViewModel : ViewModel() {

    protected val disposables = CompositeDisposable()
    var errorStringData: MutableLiveData<String> = MutableLiveData()

    protected val progressInternal = MutableLiveData<Boolean>()

    val progress: LiveData<Boolean>
        get() = progressInternal

    fun Disposable.track() {
        disposables.add(this)
    }

    fun handleError(thr: Throwable, error: String?) {
        errorStringData.value = error
    }

    override fun onCleared() {
        disposables.clear()
        super.onCleared()
    }

    protected inline fun <reified T> doSingle(
        doBlock: Single<T>,
        noinline errorBlock: (Throwable) -> Unit,
        noinline successBlock: (T) -> Unit
    ) {
        disposables.add(
            doBlock
                .threadingSubscribe()
                .subscribe(successBlock::invoke, errorBlock::invoke)
        )
    }
}