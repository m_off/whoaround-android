package ru.urentbike.app.model.network.api

import com.joyjet.data.models.response.BaseResponse
import io.reactivex.Single
import retrofit2.http.*

interface AuthApi {

    @POST("/guests")
    fun auth(@FieldMap map: Map<String, String>): Single<BaseResponse>
}