package ru.urentbike.app.model.network.auth

import com.joyjet.data.models.response.BaseResponse
import io.reactivex.Single

interface AuthRepository {

    fun auth(phone: String, smsCode: String): Single<BaseResponse>

    fun auth(refreshToken : String): Single<BaseResponse>
}