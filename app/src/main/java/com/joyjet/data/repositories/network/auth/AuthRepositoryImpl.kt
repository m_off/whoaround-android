package ru.urentbike.app.model.network.auth

import com.joyjet.data.models.response.BaseResponse
import com.joyjet.data.repositories.network.CommonConnection
import io.reactivex.Single

class AuthRepositoryImpl : AuthRepository {

    override fun auth(phone: String, smsCode: String): Single<BaseResponse> {
        val map: MutableMap<String, String> = mutableMapOf()
        map["username"] = phone
        map["password"] = smsCode
        return CommonConnection().authApi.auth(map)
    }

    override fun auth(
        refreshToken: String
    ): Single<BaseResponse> {
        val map: MutableMap<String, String> = mutableMapOf()
        map["refresh_token"] = refreshToken
        return CommonConnection().authApi.auth(map)
    }
}