package com.joyjet.data.repositories.network

import android.util.Log
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okio.Buffer
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import com.joyjet.BuildConfig
import com.joyjet.application.JoyJetApplication
import com.joyjet.data.repositories.preference.Preferences
import com.joyjet.data.repositories.preference.PreferencesImpl
import java.nio.charset.Charset
import java.util.concurrent.TimeUnit

open class ConnectionFactory {

    lateinit var prefs: Preferences

    fun getRetrofit(baseUrl: String): Retrofit = Retrofit.Builder()
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(baseUrl)
        .client(initHttpClient())
        .build()

    init {
        prefs = PreferencesImpl(JoyJetApplication.instanse)
        initInterceptor()
    }

    private fun initHttpClient(): OkHttpClient {
        val okHTTPBuilder = OkHttpClient.Builder()
        okHTTPBuilder
            .connectTimeout(2, TimeUnit.MINUTES)
            .readTimeout(2, TimeUnit.MINUTES)
            .writeTimeout(2, TimeUnit.MINUTES)
        okHTTPBuilder.addInterceptor(initHeaderInterceptor())
        if (BuildConfig.DEBUG) okHTTPBuilder.addInterceptor(initInterceptor())
        return okHTTPBuilder.build()
    }

    private fun initInterceptor() =
        Interceptor { chain ->
            val request = chain.request()
            val t1 = System.nanoTime()
            var requestBodyString: String? = null
            if (request.body != null) {
                val requestBody = request.body
                val buffer = Buffer()
                requestBody?.writeTo(buffer)
                requestBodyString = buffer.readString(Charset.forName("UTF-8"))
            }
            try {
                Log.i("OkHttp", "<--  Start request")
                Log.i(
                    "OkHttp",
                    String.format(
                        "Sending request %s %n%s",
                        request.url,
                        request.headers
                    )
                )
                Log.i("OkHttp", String.format("Sending body: %s", requestBodyString))
                Log.i("OkHttp", "End request  -->")
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

            val response = chain.proceed(request)
            var responseBodyString: String? = null
            if (response.body != null) {
                val responseBody = response.body
                val source = responseBody?.source()
                source?.request(java.lang.Long.MAX_VALUE)
                val buffer = source?.buffer()
                responseBodyString = buffer?.clone()?.readString(Charset.forName("UTF-8"))
            }
            val t2 = System.nanoTime()
            try {
                Log.i("OkHttp", "Response code: " + response.code)
                Log.i("OkHttp", "<--  Start response")
                Log.i(
                    "OkHttp",
                    String.format(
                        "Received response for %s in %.1fms%n%s",
                        response.request.url,
                        (t2 - t1) / 1e6,
                        response.headers
                    )
                )
                Log.i("OkHttp", String.format("Received body:  %s", responseBodyString))
                Log.i("OkHttp", "End response  -->")
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
            response
        }

    private fun initHeaderInterceptor() = Interceptor { chain ->
        val original = chain.request()
        val requestBuilder: Request.Builder =
            original.newBuilder().method(original.method, original.body)
        var token: String?  = original.headers.get("Authorization")
        if(token != null && !token.isEmpty()) {
            requestBuilder.header("Authorization", prefs.getToken()!!);
        }
        val request: Request = requestBuilder.build()
        chain.proceed(request)
    }
}
