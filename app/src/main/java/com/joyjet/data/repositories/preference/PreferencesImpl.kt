package com.joyjet.data.repositories.preference

import android.content.Context
import androidx.core.content.edit

class PreferencesImpl(context: Context) : Preferences {

    private val preferences = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE)

    override fun saveToken(token: String) =
        preferences.edit { putString(USER_TOKEN, "Bearer $token") }

    override fun getToken(): String? = preferences.getString(USER_TOKEN, null)

    override fun saveRefreshToken(token: String) =
        preferences.edit { putString(USER_REFRESH_TOKEN, token) }

    override fun getRefreshToken(): String? = preferences.getString(USER_REFRESH_TOKEN, null)

    companion object {
        private const val PREFERENCES = "preferences"
        private const val USER_TOKEN = "user_token"
        private const val USER_REFRESH_TOKEN = "user_refresh_token"
    }
}