package com.joyjet.data.repositories.preference

interface Preferences {

    fun saveToken(token: String)
    fun getToken(): String?

    fun saveRefreshToken(token: String)
    fun getRefreshToken(): String?
}