package com.joyjet.data.repositories.network

import com.joyjet.utils.http.HTTPUtils.Companion.getBaseUrl
import ru.urentbike.app.model.network.api.AuthApi

class CommonConnection : ConnectionFactory() {

    val authApi: AuthApi =
        getRetrofit(getBaseUrl()).create(AuthApi::class.java)
}