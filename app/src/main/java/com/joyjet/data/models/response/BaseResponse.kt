package com.joyjet.data.models.response

import com.google.gson.annotations.SerializedName

data class BaseResponse (

    @SerializedName("succeeded")
    val succeeded: Boolean,

    @SerializedName("error")
    val error: String,

    @SerializedName("errorCode")
    val errorCode: Int,

    @SerializedName("errorMessage")
    val errorMessage: String,

    @SerializedName("error_description")
    val errorDescription: String,

    @SerializedName("errors")
    val errors: ArrayList<Error>

)