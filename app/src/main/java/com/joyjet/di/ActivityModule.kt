package com.joyjet.di

import com.joyjet.presentation.authorize.authorization.AuthorizationActivity
import com.joyjet.presentation.authorize.registration.RegistrationActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector
    internal abstract fun contributeAuthorizationActivity(): AuthorizationActivity

    @ContributesAndroidInjector
    internal abstract fun contributeRegistrationActivity(): RegistrationActivity
}