package com.joyjet.di.viewModels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.joyjet.di.ViewModelFactory
import com.joyjet.presentation.authorize.authorization.AuthorizationViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey( AuthorizationViewModel::class )
    abstract fun bindAuthorizationViewModel( authorizationViewModel: AuthorizationViewModel ): ViewModel

    @Binds
    abstract fun bindViewModelFactory( factory: ViewModelFactory):
            ViewModelProvider.Factory

}