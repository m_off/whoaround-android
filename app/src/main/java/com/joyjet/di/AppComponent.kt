package com.joyjet.di

import android.app.Application
import com.joyjet.application.JoyJetApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import ru.urentbike.app.model.network.auth.AuthRepository
import javax.inject.Singleton

@Singleton
@Component(
    modules = arrayOf(
        AndroidInjectionModule::class,
        AppModule::class,
        ActivityModule::class
    )
)
interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(luaApp: JoyJetApplication)
}