package com.joyjet.di

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.joyjet.data.repositories.preference.Preferences
import com.joyjet.presentation.authorize.authorization.AuthorizationViewModel
import ru.urentbike.app.model.network.auth.AuthRepository
import javax.inject.Inject
import javax.inject.Provider
import javax.inject.Singleton

@Singleton
class ViewModelFactory(
    val authRepository: AuthRepository,
    val prefs: Preferences
) : ViewModelProvider.Factory {

    @Inject
    lateinit var authorizationViewModelProvider: Provider<AuthorizationViewModel>

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(AuthorizationViewModel::class.java)) {
            return authorizationViewModelProvider.get() as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}