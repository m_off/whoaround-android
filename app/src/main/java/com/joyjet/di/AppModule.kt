package com.joyjet.di

import android.app.Application
import dagger.Module
import dagger.Provides
import ru.urentbike.app.model.network.auth.AuthRepository
import ru.urentbike.app.model.network.auth.AuthRepositoryImpl
import javax.inject.Singleton

@Module
internal class AppModule {

    @Singleton
    @Provides
    fun provideAuthRepository(app: Application): AuthRepository {
        return AuthRepositoryImpl()
    }
}