package com.joyjet.application

import android.app.Activity
import android.app.Application
import com.joyjet.di.DaggerAppComponent
import dagger.android.DispatchingAndroidInjector
import javax.inject.Inject

class JoyJetApplication : Application() {

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()
        instanse = this
        initDI()
    }

    private fun initDI() {
        DaggerAppComponent.builder().application(this).build().inject(this)
    }

    companion object {
        lateinit var instanse: JoyJetApplication
    }
}