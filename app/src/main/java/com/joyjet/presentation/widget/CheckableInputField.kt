package com.joyjet.presentation.widget

import android.content.Context
import android.text.Editable
import android.text.TextUtils
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import com.google.android.material.textfield.TextInputEditText
import com.joyjet.R
import com.joyjet.utils.extenstions.click
import com.joyjet.utils.extenstions.gone
import com.joyjet.utils.extenstions.show

class CheckableInputField : FrameLayout, View.OnFocusChangeListener {

    private var isErrorState: Boolean = false
    private var isInFocus: Boolean = false

    private var textInputField : TextInputEditText
    private var hintText: AppCompatTextView
    private var errorText: AppCompatTextView
    private var buttonClean: AppCompatImageView
    private var borderBg: FrameLayout


    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
    }

    init {
        LayoutInflater.from(context).inflate(R.layout.checjable_input_field, this, true)
        textInputField = findViewById(R.id.textInputField)
        hintText = findViewById(R.id.hintText)
        errorText = findViewById(R.id.errorText)
        buttonClean = findViewById(R.id.buttonClean)
        borderBg = findViewById(R.id.borderBg)


        textInputField?.onFocusChangeListener = this
    }

    override fun onFocusChange(v: View?, hasFocus: Boolean) {
        isInFocus = hasFocus
        if (isInFocus)
            setFocusState()
        else
            setDefaultState()
    }

    /**
     * Метод для определения введенли какой-то текст в поле ввода.
     *
     * @return true или false в зависимости от наличия введенного текста
     */
    fun isEmpty(): Boolean = TextUtils.isEmpty(this.getText())

    /**
     * Устанавливает текст подсказски
     *
     * @param hint текст подсказски
     */
    fun setHint(hint: String) {
        hintText.text = Editable.Factory.getInstance().newEditable(hint)
    }

    /**
     * Устанавливает текст в поле ввода.
     *
     * @param text текст для ввода
     */
    fun setText(text: String) {
        textInputField.text = Editable.Factory.getInstance().newEditable(text)
    }

    /**
     * Возвращает введенный текст
     *
     * @return введенный текст
     */
    fun getText(): String {
        textInputField?.let() {
            return it.text.toString()
        }
        return ""
    }

    /**
     * Метод, через который выставляется ошибочное состояние текстового поля
     * Поля раскрашиваются в оранжевый цвет.
     *
     * @param message текст ошибки
     */
    fun setError(message: String) {
        isErrorState = true

        errorText?.apply {
            show()
            text = message
        }

        hintText?.apply {
            setTextColor(context.getColor(R.color.color_primary_orange))
            if (TextUtils.isEmpty(hint)) this.gone() else this.show()
        }
        textInputField.setTextColor(context.getColor(R.color.color_primary_orange))
        borderBg.background = context.getDrawable(R.drawable.base_orange)
        buttonClean?.apply {
            show()
            click { resetText() }
        }
    }

    /**
     * Устанавливает состояние поля, когда оно в фокусе.
     */
    private fun setFocusState() {
        isErrorState = false
        isInFocus = false
        errorText?.apply {
            gone()
            text = ""
        }
        hintText?.apply {
            setTextColor(context.getColor(R.color.color_gray))
            if (TextUtils.isEmpty(hint)) this.gone() else this.show()
        }
        textInputField.setTextColor(context.getColor(R.color.color_secondary_black))
        borderBg.background = context.getDrawable(R.drawable.base_black)
        buttonClean.gone()
    }

    /**
     * Устанавливает дефолтное состояние поля.
     */
    private fun setDefaultState() {
        isErrorState = false
        isInFocus = false
        errorText?.apply {
            visibility = View.GONE
            text = ""
        }
        hintText?.apply {
            setTextColor(context.getColor(R.color.color_gray))
            if (TextUtils.isEmpty(hint)) this.gone() else this.show()
        }
        textInputField.setTextColor(context.getColor(R.color.color_secondary_black))
        borderBg?.background = context.getDrawable(R.drawable.base_gray)
        buttonClean.gone()
    }

    /**
     * Очищает статус ошибки и введенный текст.
     */
    private fun resetText() {
        isErrorState = false
        textInputField?.apply {
            this.text = null
            this.requestFocus()
        }
        buttonClean.gone()
    }
}

