package com.joyjet.presentation.authorize.authorization

import android.content.Context
import com.joyjet.base.BaseViewModel
import com.joyjet.data.repositories.preference.Preferences
import com.joyjet.utils.updateHttpException
import ru.urentbike.app.model.network.auth.AuthRepository
import javax.inject.Inject

class AuthorizationViewModel @Inject constructor(
    val context: Context,
    val authRepository: AuthRepository,
    val prefs: Preferences
) : BaseViewModel() {

    fun startRequest() {
        doSingle(authRepository.auth(prefs.getRefreshToken()!!), {
            handleError(it, updateHttpException(it, context))
        }, { response ->
//            prefs.saveToken(response.token)
//            prefs.saveRefreshToken(response.refreshToken)
        })
    }
}