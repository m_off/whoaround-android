package com.joyjet.presentation.authorize.authorization

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.joyjet.R
import com.joyjet.databinding.AuthorizationActivityBinding
import com.joyjet.presentation.authorize.registration.RegistrationActivity
import com.joyjet.utils.extenstions.click
import com.joyjet.utils.extenstions.launchActivity

class AuthorizationActivity : AppCompatActivity() {

    private lateinit var binding : AuthorizationActivityBinding
    private lateinit var viewModel: AuthorizationViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.authorization_activity)
        initView()
        viewModel = ViewModelProvider(this).get(AuthorizationViewModel::class.java)
    }

    fun initView() {
        binding.backButton.click { finish() }
        binding.registration.click { launchActivity<RegistrationActivity>() }
        binding.button.click { viewModel.startRequest() }
    }
}