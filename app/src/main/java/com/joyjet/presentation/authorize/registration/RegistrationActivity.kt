package com.joyjet.presentation.authorize.registration

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity;
import com.google.android.material.textfield.TextInputEditText
import com.joyjet.R
import com.joyjet.databinding.RegistrationActivityBinding
import com.joyjet.utils.extenstions.click

class RegistrationActivity : AppCompatActivity() {

    private lateinit var binding: RegistrationActivityBinding
    private lateinit var viewModel: RegistrationViewModel

    lateinit var rt: TextInputEditText


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.registration_activity)

        initView()

        binding.email.setError("Some error")
        binding.nickName.setHint("hint 1")
        binding.email.setHint("hint 2")

        binding.nickName.setText("text 1")
        binding.email.setText("text 2")

//        viewModel = ViewModelProvider(
//            this,
//            ViewModelProvider.NewInstanceFactory()
//        ).get(RegistrationViewModel::class.java)

    }

    fun initView() {
        binding.backButton.click{   finish()    }
    }
}