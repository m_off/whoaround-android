package com.joyjet.utils.http

import com.joyjet.BuildConfig

class HTTPUtils {

    companion object {

        var isTestUrl = BuildConfig.DEBUG
        var baseTestUrl = "https://dev.joy-jet.com/api"
        var baseProdUrl = "https://dev.joy-jet.com/api"

        fun getBaseUrl(): String {
            if(BuildConfig.DEBUG) {
                return if (isTestUrl) baseTestUrl else baseProdUrl
            }
            return baseProdUrl
        }
    }
}