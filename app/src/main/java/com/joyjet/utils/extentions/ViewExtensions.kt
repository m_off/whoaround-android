package com.joyjet.utils.extenstions

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

fun View.gone() {
    this.visibility = View.GONE
}

fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.hide() {
    this.visibility = View.INVISIBLE
}

fun View.AllGone(vararg views: View) {
    this.visibility = View.GONE
    views.forEach { it.visibility = View.GONE }
}

fun View.AllShow(vararg views: View) {
    this.visibility = View.VISIBLE
    views.forEach { it.visibility = View.VISIBLE }
}

fun View.click(action: () -> Unit) {
    this.setOnClickListener { action.invoke() }
}

fun ViewGroup.gone(vararg views: View) {
    views.forEach { view ->
        view.visibility = View.GONE
    }
}

fun ViewGroup.show(vararg views: View) {
    views.forEach { view ->
        view.visibility = View.VISIBLE
    }
}

fun View.isVisible() = this.visibility == View.VISIBLE

fun ViewGroup.inflateView(context: Context, layout: Int) =
    LayoutInflater.from(context).inflate(layout, this, false) as ViewGroup
