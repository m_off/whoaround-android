package com.joyjet.utils.extenstions

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Parcelable

const val EXTRA = "extra.JoyJet"
const val EXTRA_STRING = "extra_string.JoyJet"
const val EXTRA_INT = "extra_int.JoyJet"
const val EXTRA_BOOLEAN = "extra_boolean.JoyJet"
const val EXTRA_LIST = "extra_list.JoyJet"
const val EXTRA_STRING_LIST = "extra_string_list.JoyJet"

inline fun <reified T : Activity> Context.launchActivity(
    toFlag: Int? = null,
    extra: Parcelable? = null,
    extraString: String? = null,
    extraInt: Int? = null,
    extraBoolean: Boolean? = null,
    extraList: ArrayList<out Parcelable>? = null,
    extraStringList: ArrayList<String>? = null
) {
    startActivity(
        createIntent<T>(
            toFlag = toFlag,
            extra = extra,
            extraString = extraString,
            extraInt = extraInt,
            extraBoolean = extraBoolean,
            extraList = extraList,
            extraStringList = extraStringList
        )
    )
}

inline fun <reified T : Activity> Context.createIntent(
    toFlag: Int? = null,
    extra: Parcelable? = null,
    extraString: String? = null,
    extraInt: Int? = null,
    extraBoolean: Boolean? = null,
    extraList: ArrayList<out Parcelable>? = null,
    extraStringList: ArrayList<String>? = null
) =
    Intent(this, T::class.java).apply {
        toFlag?.let { flags = it }
        extra?.let { putExtra(EXTRA, extra) }
        extraString?.let { putExtra(EXTRA_STRING, extraString) }
        extraInt?.let { putExtra(EXTRA_INT, extraInt) }
        extraBoolean?.let { putExtra(EXTRA_BOOLEAN, extraBoolean) }
        extraList?.let { putParcelableArrayListExtra(EXTRA_LIST, extraList) }
        extraStringList?.let { putStringArrayListExtra(EXTRA_STRING_LIST, extraStringList) }
    }

