package com.joyjet.utils

import android.content.Context
import android.os.Build
import com.google.gson.GsonBuilder
import com.joyjet.R
import com.joyjet.data.models.response.BaseResponse
import org.json.JSONException
import retrofit2.HttpException
import java.net.SocketException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.text.ParseException
import java.util.concurrent.TimeoutException
import javax.net.ssl.SSLHandshakeException



fun getDeviceName(): String {
    val manufacturer = Build.MANUFACTURER
    val model = Build.MODEL
    return if (model.startsWith(manufacturer)) {
        model.capitalize()
    } else {
        manufacturer.capitalize() + " " + model
    }
}

fun updateHttpException(t: Throwable, context: Context): String {
    try {
        if (t is SSLHandshakeException) {
            t.printStackTrace()
            return context.getString(R.string.some_network_error)
        } else if (t is TimeoutException || t is SocketTimeoutException) {
            t.printStackTrace()
            return context.getString(R.string.some_network_error)
        } else if (t is SocketException) {
            t.printStackTrace()
            return context.getString(R.string.some_network_error)
        } else if (t is UnknownHostException) {
            t.printStackTrace()
            return context.getString(R.string.some_network_error)
        } else if (t is NullPointerException) {
            t.printStackTrace()
            return context.getString(R.string.some_network_error)
        } else if (t is ParseException || t is JSONException) {
            t.printStackTrace()
            return context.getString(R.string.some_network_error)
        } else if (t is HttpException) {
            val body = t.response()!!.errorBody()
            if (t.code() == 401) {
                return "401"
            }
            try {
                val gsonBuilder = GsonBuilder()
                val gson = gsonBuilder.create()
                val errorBodyString = body!!.string()
                val response = gson.fromJson<Any>(
                    errorBodyString,
                    BaseResponse::class.java
                ) as BaseResponse
                if (response != null) {
                    if (response?.errorDescription != null && !response?.errorDescription?.isEmpty()) {
                        return response?.errorDescription
                    } else if (response?.error != null) {
                        return response?.error
                    }
                } else {
                    return ""
                }
            } catch (e: Exception) {
                e.printStackTrace()
                return context.getString(R.string.some_network_error)
            }

        } else {
            t.printStackTrace()
            return context.getString(R.string.unknown_error)
        }
    } catch (ex: Exception) {
        ex.printStackTrace()
        return context.getString(R.string.unknown_error)
    }
    return context.getString(R.string.unknown_error)
}